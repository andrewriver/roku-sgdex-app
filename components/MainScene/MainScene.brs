' ********** Copyright 2019 Roku Corp.  All Rights Reserved. **********

' Show home view
sub Show(args as object)
    ' Scene global style
    m.top.theme = m.global.theme.scene
    ' Create a Grid
    m.grid = CreateObject("roSGNode", "GridView")
    m.grid.setFields({
        style: "zoom"
        posterShape: "16x9"
        theme: m.global.theme.homeGrid
    })
    ' Content handler
    content = CreateObject("roSGNode", "ContentNode")
    content.addFields({
        HandlerConfigGrid: {
            name: m.global.constants.SGComponents.contentHandler
        }
    })
    m.grid.content = content

    m.grid.observeField("rowItemSelected", "onGridItemSelected")

    ' Display grid
    m.top.componentController.callFunc("show", {
        view: m.grid
    })
end sub


sub onGridItemSelected(event as object)
    grid = event.getRoSGNode()
    selectedIndex = event.getData()
    rowContent = grid.content.getChild(selectedIndex[0])
    ' Open details view
    detailsView = ShowDetailsView(rowContent, selectedIndex[1])
    detailsView.observeField("wasClosed", "onDetailsWasClosed")
end sub


sub onDetailsWasClosed(event as Object)
    details = event.GetRoSGNode()
    m.grid.jumpToRowItem = [m.grid.rowItemFocused[0], details.itemFocused]
end sub