' Override function from ContentHandler
sub GetContent()
    ' _feedSampleContent()
    m.api = m.global.constants.api
    m.aws = m.global.constants.aws
    request = { uri: m.api.domain + m.api.categories }
    onCategoriesFetched(fetchSync(request))
end sub


sub onCategoriesFetched(response as string)
    _ = Lodash()
    rootChildren = {
        children: []
    }
    categories = ParseJson(response)
    children = []
    for each category in categories
        request = { uri: m.api.domain + m.api.categoryVideos + category._id }
        rowAA = {
            title: category.name
            children: onCategoryVideosFetched(fetchSync(request))
        }
        rootChildren.children.push(rowAA)
    end for
    ' Populate grid content node
    m.top.content.update(rootChildren)
end sub


function onCategoryVideosFetched(response as string) as object
    children = []
    categoryVideos = ParseJson(response)
    for each video in categoryVideos
        itemNode = Utils_AAToContentNode({
            id: video._id
            title: video.title
            description: video.description
            hdPosterUrl: video.thumbnails.hd.grid
            categories: video.tags
            url: getVideoUrl(video.url)
            HDBifUrl: video.["bif-files"].HDBifUrl
        })
        children.push(itemNode)
    end for
    return children
end function


function getVideoUrl(relativePath as string) as string
    'Initialize Amazon Web server authenticated URL
    aws = AmazonWS()
    'Gets sidned resource URL
    resourceURL = aws.s3.getSignedURL({
        awsAccessKey: m.aws.access_key
        awsSecretKey: m.aws.secret_key
        bucketName: m.aws.bucket_name
        expires: m.aws.expires
        objectPath: relativePath
    })
    return resourceURL
end function


sub _feedSampleContent()
    feed = ReadAsciiFile("pkg:/feed/feed.json")
    Sleep(2000) ' to emulate API call

    if feed.Len() > 0
        json = ParseJson(feed)
        if json <> invalid ' and json.rows <> invalid and json.rows.Count() > 0
            rootChildren = {
                children: []
            }
            for each item in json
                value = json[item]
                if item = "movies" or item = "series"
                    children = []
                    for each arrayItem in value
                        itemNode = CreateObject("roSGNode", "ContentNode")
                        Utils_ForceSetFields(itemNode, {
                            hdPosterUrl: arrayItem.thumbnail
                            Description: arrayItem.shortDescription
                            id: arrayItem.id
                            Categories: arrayItem["genres"][0]
                            title: arrayItem.title
                        })
                        if item = "movies"
                            ' Add 4k option
                            'Never do like this, it' s better to check if all fields exist in json, but in sample we can skip this step
                            itemNode.Url = arrayItem.content.videos[0].url
                        end if
                        if item = "series"
                            seasonArray = []
                            for each season in arrayItem.seasons
                                episodeArray = []
                                for each episode in season.episodes
                                    episodeNode = CreateObject("roSGNode", "ContentNode")

                                    episodeNode.SetFields({
                                        title: episode.title
                                        url: episode.content.videos[0].url
                                        hdPosterUrl: episode.thumbnail
                                        Description: episode.shortDescription
                                    })

                                    episodeArray.Push(episodeNode)
                                end for
                                seasonArray.Push(episodeArray)
                            end for
                            Utils_ForceSetFields(itemNode, { "seasons": seasonArray })
                        end if
                        children.Push(itemNode)
                    end for

                    rowAA = {
                        title: item
                        children: children
                    }
                    rootChildren.children.Push(rowAA)
                end if
            end for
            m.top.content.Update(rootChildren)
        end if
    end if
end sub