' ********** Copyright 2019 Roku Corp.  All Rights Reserved. **********

function ShowDetailsView(content as object, index as integer, isContentList = true as boolean) as object
    ' Create an DetailsView Object
    details = CreateObject("roSGNode", "DetailsView")
    ' Observe the content, so that when it is set the callback
    ' function will run and the buttons can be created
    details.observeField("currentItem", "onDetailsContentSet")
    details.observeField("buttonSelected", "onButtonSelected")
    details.setFields({
        content: content
        jumpToItem: index
        isContentList: isContentList
    })

    ' This will cause the View to be shown on the View
    m.top.ComponentController.callFunc("show", {
        view: details
    })
    return details
end function


sub onDetailsContentSet(event as object)
    details = event.getRoSGNode()
    currentItem = event.getData()
    if currentItem <> invalid
        buttonsToCreate = []

        if currentItem.url <> invalid and currentItem.url <> ""
            buttonsToCreate.push({ title: "Play", id: "play" })
        end if

        if buttonsToCreate.count() = 0
            buttonsToCreate.push({ title: "No Content to play", id: "no_content" })
        end if
        btnsContent = CreateObject("roSGNode", "ContentNode")
        btnsContent.update({ children: buttonsToCreate })
    end if
    details.buttons = btnsContent
end sub


sub onButtonSelected(event as Object)
    details = event.getRoSGNode()
    selectedButton = details.buttons.getChild(event.getData())

    if selectedButton.id = "play"
        OpenVideoPlayer(details.content, details.itemFocused, details.isContentList)
    else
        ' handle all other button presses
    end if
end sub