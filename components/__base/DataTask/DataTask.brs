sub init()
    m.port = createObject("roMessagePort")
    ' Stores the content if not all requests are ready
    m.cache = createObject("roSGNode", "ContentNode")
    m.launchArgs = {}
    m.appendMode = false
    ' setting callbacks for url request and response
    m.top.observeField("input", m.port)
    m.top.observeField("clearQueue", m.port)
    ' setting the task thread function
    m.top.functionName = "go"
    m.top.control = "RUN"
end sub

'Task function
sub go()
    ' Holds requests by id
    m.jobsById = {}
    m.queue = []
    ' UriFetcher event loop
    while true
        msg = wait(0, m.port)
        mt = type(msg)
        ' If a request was made
        if mt = "roSGNodeEvent"
            if msg.getField() = "input"
                m.launchArgs = m.top.launchArgs
                m.appendMode = m.top.appendMode
                rqWrapper = msg.getData()
                request = rqWrapper.request
                content = getContent(request)
                if isValid(content)
                    if request.uri <> "" then cacheId = request.uri else cacheId = request.url
                    ? "💡 💡  ...cache: @";cacheId
                    request.response = content
                else
                    ? "☁️  ...request: ";request.url
                    if not addRequest(request)
                        handleError("Invalid request", "", request)
                    end if
                end if
            else if msg.getField() = "clearQueue"
                ? "🆑  clearing queue"
                clearQueue = msg.getData()
                if clearQueue
                    while m.queue.count() > 0
                        cancelCurrentJob()
                    end while
                end if
            else
                handleError("Unrecognized field", msg.getField())
            end if
        ' If a response was received
        else if mt="roUrlEvent"
            processResponse(msg)
        ' Handle unexpected cases
        else
            handleError("Unrecognized event type", mt)
        end if
    end while
end sub


function addRequest(request as Object) as Boolean
    if type(request) = "roSGNode"
        cancel = request.cancel
        headers = request.headers
        method = request.method
        body = formatJSON(request.body)
        url = request.url
        if type(url) = "roString"
            ' cancel last request
            if type(cancel) = "roBoolean" and cancel
                cancelCurrentJob()
            end if
            ' proceed'
            urlXfer = getUrlTransfer(url, headers)
            ' should transfer more stuff from request to urlXfer
            idKey = stri(urlXfer.getIdentity()).trim()
            'Make request based on request method
            ' AsyncGetToString returns false if the request couldn't be issued
            if method = "POST" or method = "PUT" or method = "DELETE"
                urlXfer.setRequest(method)
                ok = urlXfer.asyncPostFromString(body)
            else
                ok = urlXfer.asyncGetToString()
            end if
            if ok
                m.jobsById[idKey] = {
                    request: request
                    xfer: urlXfer
                }
                m.queue.push(idKey)
            else
                handleError("Request couldn't be issued", "", request)
            end if
            ? "☁️  transfer '"; idkey; "' for URI '"; request.uri; "'"; " succeeded: "; ok
        else
            handleError("Invalid uri", url, request)
        end if
    else
        handleError("Request is the wrong type", type(request), request)
        return false
    end if

    return true
end function


function getUrlTransfer(url as String, headers as Dynamic) as Object
    urlXfer = createObject("roUrlTransfer")
    urlXfer.setCertificatesFile("common:/certs/ca-bundle.crt")
    urlXfer.initClientCertificates()
    urlXfer.setUrl(url)
    urlXfer.setPort(m.port)
    ' Add headers to the request
    if isValid(headers) then urlXfer.setHeaders(headers)

    return urlXfer
end function


sub cancelCurrentJob()
    currentJobId = m.queue.pop()
    ' no job, no cancel
    if currentJobId = invalid then return
    ' cancel it
    job = m.jobsById[currentJobId]
    if job = invalid then ?"🔕  Job invalid:";currentJobId:return
    job.xfer.asyncCancel()
    ?"🔕  Job canceled:";currentJobId
    m.jobsById.delete(currentJobId)
end sub


'Received a response
sub processResponse(msg as Object)
    idKey = stri(msg.getSourceIdentity()).trim()
    job = m.jobsById[idKey]
    if isValid(job)
        ?"☁️  Response for transfer '"; idkey; "' for URI '"; job.request.uri; "'"
        result = {
            code:    msg.getResponseCode()
            headers: msg.getResponseHeaders()
            content: msg.getString()
        }

        m.jobsById.delete(idKey)
        m.queue.pop()
        job.response = result
        code = msg.getResponseCode()
        if code = 200 or code = 204
            parseResponse(job)
        else
            handleError("Status code was", msg.getResponseCode(), job.request)
        end if
    else
        handleError("Event for cancelled job", idkey, job.request)
    end if
end sub

' For loading content
sub parseResponse(job as Object)
    rq = job.request
    processor = getDataProcessor(rq.responseType)
    props = {}
    
    if isValid(rq.props) then props.append(rq.props)
    if isValid(m.launchArgs) then props.append(m.launchArgs)

    job.response.content = processor(job.response.content, props)
    addContentToCache(job.response, rq)
end sub


sub addContentToCache(response as Object, request as Object)
    ' url or id
    if request.uri = "" then key = trunc(request.url) else key = request.uri
    data = response.content

    if m.cache.hasField(key) and m.appendMode
        processedData = getSanitizedFields(m.cache[key])
    else
        processedData = {}
    end if

    if isArray(data)
        if isInvalid(processedData.items) then processedData.items = []
        if isInvalid(processedData.dict) then processedData.dict = {}
        processedData.items.append(data)
        processedData.dict.append(Lodash().reduce(data, dictReducer, {}))
        m.cache.appendChildren(data)
    else if isAssocArray(data)
        processedData.append(data)
    else
        handleError("Data format not recognized!", type(data), request)
        return
    end if
    ' update fields
    if m.cache.hasField(key)
        contentItem = m.cache[key]
        contentItem.setFields(processedData)
    else
        ' convert to content node
        contentItem = createObject("roSGNode", "ContentNode")
        contentItem.addFields({
            headers: response.headers
            content: processedData
        })
        contentItem.setFields({ id: key })
        ' if ignore cache is true, no response will be saved in cache
        if request.cache
            cacheFieldsUpdate = {}
            cacheFieldsUpdate[key] = contentItem
            m.cache.addFields(cacheFieldsUpdate)
            m.cache.appendChild(contentItem)
            ?"🔥 🔥  Adding item [";key;"] to main cache"
            m.top.cache = m.cache
        end if
    end if
    request.response = contentItem
end sub


function dictReducer(dict as Object, item as Object, idx as Integer, props = {} as Object) as Object
    ?"dict=";dict;" item=";item;" idx=";idx;" props=";props
    id = Lodash().get(item, "_id", "").toStr()
    if id = "" then return dict
    dict[id] = item

    return dict
end function


function getContent(request as Object) as Dynamic
    rq = request
    ' go for a fetch
    if not isNode(rq) or (rq.url = "") or not rq.cache then return invalid
    ' return cache data
    if rq.uri = "" then key = trunc(rq.url) else key = rq.uri
    
    return m.cache[key]
end function


function trunc(key as String, length = 128 as Integer) as String
    ' there is a limit for the lenght that keys can have 
    ' and it is known that length of keys impacts performance
    ' setting 128 as default but it can change in the future
    return right(key, length)
end function


sub handleError(errorMsg as String, reason = invalid as Dynamic, request = invalid as Dynamic)
    ?"❌  Error - ";errorMsg;" : ";reason
    if isNode(request)
        request.response = getErrorResponse(request, errorMsg, reason)
    end if
end sub


function getErrorResponse(request as Object, errorMsg as String, reason = invalid as Dynamic) as Object
    error = createObject("roSGNode", "Error")
    r = ""
    if isValid(reason) then r = reason.toStr()
    
    error.setFields({
        description: ""
        src: errorMsg + " : " + r
        retry: { request: request.context }
    })

    return error
end function