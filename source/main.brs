' ********** Copyright 2019 Roku Corp.  All Rights Reserved. **********

' Start SGDEX channel by returning name of the scene that extends BaseScene
function GetSceneName() as string
    screen = createObject("roSGScreen")
    globalNode = screen.getGlobalNode()
    ' add config nodes accesible from global
    configNodes = getConfigNodesData()
    if configNodes.nodes.count() > 0
        globalNode.appendChildren(configNodes.nodes)
        globalNode.addFields(configNodes.fields)
    end if
    return globalNode.constants.SGComponents.baseScene
end function


' config files loader
function getConfigNodesData(configPath = "pkg:/source/config" as string) as object
    config = {
        nodes: []
        fields: {}
    }
    configFiles = listDir(configPath)

    for each fileName in configFiles
        props = getJSONFromPath(configPath + "/" + fileName)
        node = createObject("roSGNode", "ContentNode")
        node.id = fileName.split(".")[0]
        node.addFields(props)
        config.nodes.push(node)
        config.fields[node.id] = node
    end for

    return config
end function


function getJSONFromPath(configFileName as string) as object
    ' path to locate config files
    initConfigStr = readAsciiFile(configFileName)
    initConfigObj = parseJson(initConfigStr)

    return initConfigObj
end function