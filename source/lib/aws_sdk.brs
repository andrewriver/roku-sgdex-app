' Deals with Amazon Web services authorization
' Be aware - Designed as Singleton so only one instance is created under m.amazonAws
' @Author Andres Garcia, Zemoga Inc - <andres@zemoga.com>
' @version v.0.5.0 - Work in progress to add additional AWS-S3 features

function AmazonWS() as Object

    if m.amazonAws = Invalid

        m.amazonAws = {

            'specific S3 opperations
            s3: {
                'Generate a keyed hash value using the HMAC method
                ' @param {string} parameters.awsAccessKey - Your AWS Access key
                ' @param {string} parameters.awsSecretKey - Your AWS secret key
                ' @param {string} parameters.bucketName - Name of the AWS bucket
                ' @param {string} parameters.objectPath - Name of the file/resource
                ' @param {string} [parameters.expires=5] - Expiration time of the URL in minutes
                __hash_hmac: function(messsage As String, secretKey as String) as String

                    hmac = CreateObject("roHMAC")
                    privateKeyByteArray = createObject("roByteArray")
                    privateKeyByteArray.fromAsciiString(secretKey)
                    result = hmac.setup("sha1", privateKeyByteArray)

                    if result = 0
                        messageByteArray = createObject("roByteArray")
                        messageByteArray.fromAsciiString(messsage)
                        result = hmac.process(messageByteArray)
                        return result.toBase64String()
                    end if

                    return ""
                end function

                ' Gets an Authenticated request URL with Query String Authentication
                ' @param {Object} parameters - The object containing the required parameters
                ' @param {string} parameters.awsAccessKey - Your AWS Access key
                ' @param {string} parameters.awsSecretKey - Your AWS secret key
                ' @param {string} parameters.bucketName - Name of the AWS bucket
                ' @param {string} parameters.objectPath - Name of the file/resource
                ' @param {string} [parameters.expires=5] - Expiration time of the URL in minutes
                getSignedUrl: function(parameters as Object) as String

                    _ = lodash()

                    awsAccessKey = parameters.awsAccessKey
                    awsSecretKey = parameters.awsSecretKey
                    bucketName = parameters.bucketName
                    objectPath = parameters.objectPath

                    'Relies on lodash library to set a default value from object property if it is Invalid
                    'If you don't have the library just swtich to expires = parameters.expires
                    expires = _.get(parameters, "expires", 5)

                    ' ?"awsAccessKey = ";awsAccessKey
                    ' ?"awsSecretKey = ";awsSecretKey
                    ' ?"bucketName = ";bucketName
                    ' ?"objectPath = ";objectPath
                    ' ?"expires = ";expires

                    'Calculating expiry time
                    expires = CreateObject("roDateTime").asSeconds() + (expires * 60)
                    signature = "GET" + Chr(10) + Chr(10) + Chr(10) + expires.toStr() + Chr(10) + "/" + bucketName + "/" + objectPath

                    'Calculating  HMAC-sha1 and URL-Encoding
                    hashedSignature = m.__hash_hmac(signature, awsSecretKey).Escape()

                    'Constructing the URL
                    url = "https://" + bucketName + ".s3.amazonaws.com/" + objectPath

                    'Constructing the query String
                    url += "?AWSAccessKeyId=" + awsAccessKey + "&Expires=" + expires.toStr() + "&Signature=" + hashedSignature

                    return url
                end function
            }
        }
    end if

    return m.amazonAws
end function
