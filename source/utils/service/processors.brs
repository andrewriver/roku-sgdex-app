' requires ../source/utils/common/nodes.brs
' requires ../source/utils/common/typeChecking.brs
' requires ../source/lib/lodash.brs

function getDataProcessor(processorName as String) as Object
    ' declare custom processors at end of file and add them here
    ' its identifier should ideally be the same as the request identifier/name
    processorDictionary = {
        json: json
    }

    return Lodash().get(processorDictionary, processorName, _noOp)
end function


'-------------------------------------------------------------------------------
' base processors
function __jsonPrcsr(jsonStr as String) as Object
    if jsonStr = "" then jsonStr = formatJSON({ id: "" })
    return parseJson(jsonStr)
end function


function __xmlPrcsr(xmlStr as String, keyReplacements = {} as Object) as Object
    xml = createObject("roXMLElement")
    xml.parse(xmlStr)
    fields = {}
    fields.append(_xmlNodeToAssocArray(xml, keyReplacements))

    return fields
end function


'-------------------------------------------------------------------------------
' private funcs
function _xmlNodeToAssocArray(rootNode as Object, keyReplacements as Object) as Object
    _ = Lodash()
    children = rootNode.getChildElements()
    defaultKey = rootNode.getName()
    key = _.get(keyReplacements, defaultKey, defaultKey)
    result = {}
    if children <> invalid
        result[key] = _.reduce(children.toArray(), _xmlConverterReducer, {}, { keyReplacements: keyReplacements})
    else
        result[key] = rootNode.getText()
    end if

    return result
end function


function _xmlConverterReducer(rootNode as Dynamic, childNode as Dynamic, idx as Integer, props as Object) as Object
    result = {}
    result.append(rootNode)
    result.append(_xmlNodeToAssocArray(childNode, props.keyReplacements))

    return result
end function


function _noOp(dataStr as String, props = {} as Dynamic) as Object
    return { dataStr: dataStr }
end function


'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
'    ADD CUSTOM PROCESSORS BELOW ↓↓↓
'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
function json(dataStr as String, props = {} as Dynamic) as Object
    return __jsonPrcsr(dataStr)
end function