' requires:
' - source/utils/common/typeChecking.brs
' - source/utils/common/thenable.brs
function fetch(rawUrl as string, requestData = {} as object) as object
    ' no data task set? initialize it
    if isInvalid(m.global.dataTask) then initDataTask()
    ' create base props
    request = __getDefaultRequest()
    ' override defaults
    request.append(requestData)
    request.url = __proxyUrl(getParamsUrl(rawUrl, request.params))
    requestNode = createObject("roSGNode", "Node")
    'adding empty fields w/out default value
    requestNode.addField("response", "node", false)
    requestNode.addFields(request)
    ' set request in data
    m.global.dataTask.setFields({
        input: { request: requestNode }
    })

    return getThenable(requestNode, "response")
end function


function fetchSync(requestData = {}) as string
    urlXfer = createObject("roUrlTransfer")
    urlXfer.setCertificatesFile("common:/certs/ca-bundle.crt")
    urlXfer.initClientCertificates()
    urlXfer.setUrl(requestData.uri)
    res = urlXfer.getToString()
    return res
end function


' private fetch member func - getter for default request structure
function __getDefaultRequest() as object
    return {
        url: ""
        method: "GET" 'GET, POST, PUT, DELETE, etc.
        cache: true ' true/false instead of strings as used in JS
        headers: {},
        uri: "" ' resource id (cache purposes - it replaces url as cache key)
        params: {} ' used for query string replacements
        body: {}
        props: {} ' used to pass properties to task thread
        cancel: false 'cancel any previous request in process
        responseType: ""
    }
end function


function getParamsUrl(url as string, params = {} as object) as string
    if not isAssocArray(params) or params.count() = 0 then return url
    finalUrl = url
    queryString = ""
    paramList = []
    for each param in params
        tmpUrl = finalUrl
        value = params[param].toStr()
        value = value.encodeUriComponent()
        finalUrl = replaceTokens(finalUrl, "\{" + param + "\}", value)
        if tmpUrl = finalUrl then paramList.push(param + "=" + value)
    end for

    if paramList.count() > 0 then queryString = "?" + paramList.join("&")

    return finalUrl + queryString
end function


function addItemToCache(item as object) as boolean
    key = item.id
    ' invalid key? generate random one
    if isInvalid(key) or key = ""
        devInfo = CreateObject("roDeviceInfo")
        key = devInfo.getRandomUUID()
    end if

    ?"🔥  Adding item [";key;"] to render thread cache"
    return addRegisterChild(m.global.itemsCache, item, key)
end function


sub initDataTask(launchArgs = {})
    if isValid(m.global.dataTask) then return

    dataTask = m.global.createChild("DataTask")
    dataCache = m.global.createChild("ContentNode")
    m.global.addFields({
        dataTask: dataTask
        dataCache: dataCache
    })
    dataTask.setFields({ launchArgs: launchArgs })
end sub


sub cancelAllRequests()
    if isInvalid(m.global.dataTask) then return
    ? "[fetch] - cancel"
    m.global.dataTask.setFields({ clearQueue: true })
end sub


function getAssocArrayFromQuery(query as string, separator = "&" as string, assignator = "=" as string, keyReplacements = {} as object) as object
    values = {}
    regex = createObject("roRegex", substitute("^(.+?{0}[^{1}{0}]+({1}|$))+$", assignator, separator), "i")
    if not regex.isMatch(query) then return values
    for each pair in query.tokenize(separator)
        valueKey = pair.tokenize(assignator)
        key = valueKey[0]
        if isAssocArray(keyReplacements[key]) then key = keyReplacements[key]
        values[key] = valueKey[1]
    end for
    return values
end function


function __proxyUrl(url as string) as string
    proxy = Lodash().get(m.global, "constants.proxyUrl")
    if not isString(proxy) or proxy = "" then return url
    protocolLen = url.instr("://")
    protocol = url.left(protocolLen)

    return "http://" + proxy + "/" + protocol + "/" + url.right(url.len() - protocolLen - 3)
end function