' ------------------------------------------------------------------------------
' Invalid-checking Utilities
'

function isInvalid(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "Invalid" OR valType = "roInvalid"
end function


function isValid(val as Dynamic) as Boolean
    return NOT isInvalid(val)
end function


function isInitialized(val as Dynamic) as Boolean
    valType = lCase(type(val))
    return NOT valType = "<uninitialized>"
end function

' If an integer is the field of a node, its type is `roInt`.
' If an integer is the value of an array or an assoc array, its type is `roInteger`,
'   UNLESS the value was first the field of node, in which case its type will be `roInt`.
function isInteger(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "Integer" OR valType = "roInt" OR valType = "roInteger"
end function


function isString(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "String" OR valType = "roString"
end function


function isLongInteger(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "LongInteger" OR valType = "roLongInteger"
end function


function isFloat(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "Float" OR valType = "roFloat"
end function


function isDouble(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "Double" OR valType = "roDouble"
end function


function isBoolean(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "Boolean" OR valType = "roBoolean"
end function


function isArray(val as Dynamic) as Boolean
    valType = type(val)
    return  valType = "roArray"
end function


function isAssocArray(val as Dynamic) as Boolean
    valType = type(val)
    return  valType = "roAssociativeArray"
end function


function isFunction(val as Dynamic) as Boolean
    valType = type(val)
    return  valType = "Function" OR valType = "roFunction"
end function


function isNode(val as Dynamic) as Boolean
    valType = type(val)
    return valType = "roSGNode"
end function


function isEqual(val1 as Dynamic, val2 as Dynamic) as Boolean
    if type(val1) <> type(val2) then return false
    if isAssocArray(val1) or isArray(val1)
        return formatJson(val1) = formatJson(val2)
    else
        return val1 = val2
    end if
end function


function isErrorNode(value as Dynamic) as Boolean
    return isNode(value) and (uCase(value.subtype()) = "ERROR")
end function