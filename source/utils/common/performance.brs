' perf helper
function tick(msg = "" as Dynamic, timer = invalid as Dynamic, prefix = "🚀 " as String, prePadding = 0 as Integer, postPadding = 0 as Integer) as Object
    if isInvalid(timer) then timer = createObject("roTimespan")
    ?prefix;" : ";msg;": ";timer.totalMilliseconds()
    timer.mark()

    return timer
end function