' requires:
' - source/utils/common/typeChecking.brs
function getThenable(target as Object, doneFieldName = "done" as String) as Dynamic
    if not isNode(target) or not target.hasField(doneFieldName)
        ?"❌  Error: target object is not valid as a thenable": return invalid
    end if

    target.addField("__thnblDone", "node", false)
    target.id = __thblGetRandomId()
    
    return {
        __: {
            fieldName: doneFieldName
            this: m
            target: target
        }
        thn: function (callbackHandler as Object, thenRef = "" as String)
            this =  m.__.this
            target = m.__.target
            target.addFields({ _thenRef: thenRef })
            cbId = target.id + "_callback"
            oeId = target.id + "_onError"
            field = m.__.fieldName
            this[cbId] = callbackHandler
            this[oeId] = __thnblNoOp

            if isValid(target[field])
                target.unobserveFieldScoped(field)
                __thnblProcessThenable(target, field)
            else
                target.observeFieldScoped(field, "__thnblThenResolver")
            end if

            return {
                __: {
                    this: this
                    target: target
                }
                catch: sub (onErrorHandler as Object)
                    this =  m.__.this
                    target = m.__.target
                    oeId = target.id + "_onError"
                    this[oeId] = onErrorHandler
                end sub
            }
        end function
    }
end function


' private member sub - generic error
sub __thnblNoOp(errorContent = invalid as Dynamic)
    'do nothing
end sub


' private member func - get base observable for thenables
function __thnblGetBaseThenable(fieldData = {} as Object, doneFieldName = "done" as String) as Object
    baseThenable = createObject("roSGNode", "Node")
    if isAssocArray(fieldData) then baseThenable.addFields(fieldData)
    ' in case a field passed along data have the same `done` field name, remove it first
    if baseThenable.hasField(doneFieldName) then baseThenable.removeField(doneFieldName)
    baseThenable.addField(doneFieldName, "node", false)

    return baseThenable
end function


' private member func - get random id
function __thblGetRandomId(prefix = "__thnbl_" as String) as String
    if isInvalid(m.__devInfo) then  m.__devInfo = createObject("roDeviceInfo")
    return  prefix + m.__devInfo.getRandomUUID()
end function


' private member func - `then`resolver for fetch promise
sub __thnblThenResolver(fieldEvt as Object)
    thenable = fieldEvt.getRoSGNode()
    field = fieldEvt.getField()
    thenable.unobserveFieldScoped(field)
    __thnblProcessThenable(thenable, field)
end sub


' private member func - thenable processor
sub __thnblProcessThenable(thenable as Object, field as String)
    id = thenable.id
    result = thenable[field]
    ' errors go to catch
    if isInvalid(result) or isErrorNode(result)
        cbId = id + "_onError"
    else
        cbId = id + "_callback"
    end if 

    cb = m[cbId]
    if isFunction(cb)
        cb(result)
    else if isString(cb)
        thenable.observeFieldScoped("__thnblDone", cb)
        thenable.__thnblDone = result
        thenable.unobserveFieldScoped("__thnblDone")
    end if
    ' remove references from m
    thenable = invalid
    m.delete(cbId)
end sub


' related utils
function isThenable(obj as Object) as Boolean
    return implementsAssocArray(obj) and obj.doesExist("thn") and isFunction(obj.thn)
end function


function getThenableAll(thenableArray as Object) as Object
    ' not an array? return what it was passed as a parameter
    if not isArray(thenableArray) then return invalid

    node = __thnblGetBaseThenable({ count: 0, queue: [] })
    id = __thblGetRandomId()
    m[id] = node
    thenableCounter = 0
    node.id = id
    ' process all thenables
    for each thenable in thenableArray
        if isThenable(thenable)
            thenable.thn("__thnblSolveQueue", id).catch("__thnblCatchErrorInQueue")
            thenableCounter = thenableCounter + 1
        end if
    end for
    if thenableCounter = 0 then return invalid
    node.setFields({ count: thenableCounter })

    return getThenable(node)
end function


' private member func (all) - catch error and pass it as root response
sub __thnblCatchErrorInQueue(fieldEvt as Object)
    result = fieldEvt.getData()
    id = fieldEvt.getRoSGNode()._thenRef
    node = m[id]
    if isValid(node) then node.done = result
    m.delete(id)
end sub


' private member func (all) - accumulate responses into a thenable
sub __thnblSolveQueue(fieldEvt as Object)
    result = fieldEvt.getData()
    id = fieldEvt.getRoSGNode()._thenRef
    node = m[id]
    maxCount = node.count
    queue = []
    queue.append(node.queue)
    queue.push(result)

    if queue.count() = maxCount
        response = createObject("roSGNode", "ContentNode")
        response.appendChildren(queue)
        node.done = response
        m.delete(id)
    else
        node.queue = queue
    end if 
end sub